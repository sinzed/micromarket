import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { HeaderModule} from 'src/app/header/header.module';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header/header.component';

export const APP_ROUTES: Routes = [
  {
    path: '',
    loadChildren: () => import('./header/header.module').then(m => m.HeaderModule)
  }
];


@NgModule({
  imports: [
    BrowserModule,
    HeaderModule,
    FormsModule,
    RouterModule.forRoot(APP_ROUTES),
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
  ],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
