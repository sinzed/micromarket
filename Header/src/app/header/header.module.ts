import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HEADER_ROUTES } from './header.routes';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(HEADER_ROUTES)
  ],
  declarations: [
    HeaderComponent
  ]
})
export class HeaderModule { }
