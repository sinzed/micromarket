import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routes';
import { Mfe1FacebookComponent } from './remote-frontends/mfeFacebook.component';
import { MfeInstgramComponent } from './remote-frontends/mfeInstgram.component';
import { MfeLoginComponent } from './remote-frontends/mfeLogin.component';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { MfeHeaderComponent } from './remote-frontends/mfeHeader.component';
import { MfeMatCardComponent } from './remote-frontends/mfeMatCard.component';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    Mfe1FacebookComponent,
    MfeLoginComponent,
    MfeInstgramComponent,
    MfeHeaderComponent,
    MfeMatCardComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
