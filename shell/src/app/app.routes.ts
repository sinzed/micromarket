import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MfeLoginComponent } from './remote-frontends/mfeLogin.component';
import { Mfe1FacebookComponent } from './remote-frontends/mfeFacebook.component';
import { loadRemoteModule } from '@angular-architects/module-federation';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';


export const APP_ROUTES: Routes = [
    {
      path: '',
      component: HomeComponent,
      pathMatch: 'full'
    },
 

    {
      path: 'login',
      loadChildren: () =>
      loadRemoteModule({
          remoteEntry: environment.mfeLoginUrl,
          remoteName: 'mfeLogin',
          exposedModule: './Module'
      })
      .then(m => m.LoginModule)
}
];
