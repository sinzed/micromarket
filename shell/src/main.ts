import { loadRemoteEntry } from '@angular-architects/module-federation';
import { environment } from './environments/environment';

Promise.all([
   loadRemoteEntry(environment.mfeLoginUrl, 'mfeLogin'),
   loadRemoteEntry(environment.mfeHeader, 'mfeHeader'),
   loadRemoteEntry(environment.mfeFacebookUrl, 'mfeFacebook'),
   loadRemoteEntry(environment.mfeInstagramUrl, 'mfeInstagram'),
   loadRemoteEntry(environment.mfeMaterial, 'mfeMaterial'),
])
.catch(err => console.error('Error loading remote entries', err))
.then(() => import('./bootstrap'))
.catch(err => console.error(err));
